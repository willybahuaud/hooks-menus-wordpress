<?php
/**
 * ajout de classes
 */
add_action( 'nav_menu_css_class', 'menu_item_classes', 10, 3 );
function menu_item_classes( $classes, $item, $args ) {
    // Gardons seulement les classes qui nous intéressent
    $classes = array_intersect( $classes, array( 
                               'menu-item', 
                               'current-menu-item', 
                               'current-menu-parent', 
                               'current-menu-ancestor', 
                               'menu-item-has-children' 
                               ) );
    // Ajoutons la classe pour désigner les éléments vides
    if ( "#" == $item->url ) {
        $classes[] = 'empty-item';
    }
    // Si nous sommes sur une page single d'un CPT (ici my_cpt)...
    // ... Ajoutons la classe current-menu-parent sur l'item correspondant à son archive
    if ( is_singular( 'my_cpt' ) && get_post_type_archive_link( 'my_cpt' ) == $item->url ) {
        $classes[] = 'current-menu-ancestor';
    }
    return $classes;
}