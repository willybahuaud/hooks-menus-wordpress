<?php
/**
 * Transient menu
 */
// Set transient
add_filter( 'wp_nav_menu', 'set_transient_nav_menu', 10, 2 );
function set_transient_nav_menu( $nav_menu, $args ) {
    // La taille d'un md5 est de 32 caractères
    // Celle du nom d'un transient ne peut dépasser 45
    $transient_name = 'nav_' . md5( serialize( $args ) );
    set_transient( $transient_name, $nav_menu );
    return $nav_menu;
}

// Get transient
add_filter( 'pre_wp_nav_menu', 'get_transient_nav_menu', 10, 2 );
function get_transient_nav_menu( $pre, $args ) {
    $transient_name = 'nav_' . md5( serialize( $args ) );
    if ( false !== ( $t = get_transient( $transient_name ) ) ) {
        $pre = $t;
    }
    return $pre;
}

// Flush transient
add_action( 'wp_update_nav_menu', 'delete_transients_nav_menu' );
add_action( 'wp_delete_nav_menu', 'delete_transients_nav_menu' );
function delete_transients_nav_menu() {
    // On supprime tous les transients dès qu'un menu est mis à jour
    // Pas moyen de faire dans la dentèle...
    // ... car difficilement possible de connaitre l'ID du menu au moment du hook wp_nav_menu
    global $wpdb;
    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '\_transient\_nav\_%'" );
}

if ( ! is_404() ) {
	$transient_name = 'nav_' . md5( serialize( $args ) . $_SERVER["REQUEST_URI"] );
	//...