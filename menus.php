<?php
/**
 * Transient menu
 */
// Set transient
add_filter( 'wp_nav_menu', 'set_transient_nav_menu', 10, 2 );
function set_transient_nav_menu( $nav_menu, $args ) {
    // La taille d'un md5 est de 32 caractères
    // Celle du nom d'un transient ne peut dépasser 45
    $transient_name = 'nav_' . md5( serialize( $args ) );
    set_transient( $transient_name, $nav_menu );
    return $nav_menu;
}

// Get transient
add_filter( 'pre_wp_nav_menu', 'get_transient_nav_menu', 10, 2 );
function get_transient_nav_menu( $pre, $args ) {
    $transient_name = 'nav_' . md5( serialize( $args ) );
    if ( false !== ( $t = get_transient( $transient_name ) ) ) {
        $pre = $t;
    }
    return $pre;
}

// Flush transient
add_action( 'wp_update_nav_menu', 'delete_transients_nav_menu' );
add_action( 'wp_delete_nav_menu', 'delete_transients_nav_menu' );
function delete_transients_nav_menu() {
    // On supprime tous les transients dès qu'un menu est mis à jour
    // Pas moyen de faire dans la dentèle...
    // ... car difficilement possible de connaitre l'ID du menu au moment du hook wp_nav_menu
    global $wpdb;
    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '\_transient\_nav\_%'" );
}

if ( ! is_404() ) {
	$transient_name = 'nav_' . md5( serialize( $args ) . $_SERVER["REQUEST_URI"] );
	//...

/**
 * ajout de classes
 */
add_action( 'nav_menu_css_class', 'menu_item_classes', 10, 3 );
function menu_item_classes( $classes, $item, $args ) {
    // Gardons seulement les classes qui nous intéressent
    $classes = array_intersect( $classes, array( 
                               'menu-item', 
                               'current-menu-item', 
                               'current-menu-parent', 
                               'current-menu-ancestor', 
                               'menu-item-has-children' 
                               ) );
    // Ajoutons la classe pour désigner les éléments vides
    if ( '#' == $item->url ) {
        $classes[] = 'empty-item';
    }
    // Si nous sommes sur une page single d'un CPT (ici my_cpt)...
    // ... Ajoutons la classe current-menu-parent sur l'item correspondant à son archive
    if ( is_singular( 'my_cpt' ) && get_post_type_archive_link( 'my_cpt' ) == $item->url ) {
        $classes[] = 'current-menu-ancestor';
    }
    return $classes;
}

/**
 * Ajout d'un élément avant le menu
 */
add_action( 'wp_nav_menu', 'responsive_menu_button', 9, 2 );
function responsive_menu_button( $menu, $args ) {
	// S'il s'agit du menu principal, j'ajoute mon bouton devant
	if ( 'menu_primary' == $args->theme_location ) {
		$menu = '<button class="switch-menu" type="button">Menu</button>' . $menu;
	}
	return $menu;
}

/**
 * Changer les liens vides en span
 */
add_filter( 'walker_nav_menu_start_el', 'empty_nav_links_to_span', 10, 4 );
function empty_nav_links_to_span( $item_output, $item, $depth, $args ) {
	if ( '#' == $item->url ) {
		$item_output = preg_replace( '/<a.*?>(.*)<\/a>/', '<span>$1</span>', $item_output );
	}
	return $item_output;
}

/**
 * Ouvrir les liens externes dans une nouvelles fenêtre
 */
add_filter( 'nav_menu_link_attributes', 'open_external_nav_link_new_window' );
function open_external_nav_link_new_window( $atts ) {
	// Si l'URL ne commence pas par mon domaine
	if ( ! preg_match( '/^http:\/\/example\.com/', $atts['href'] ) ) {
		$atts['target'] = '_blank';
		$atts['title']  = "Ce lien s'ouvrira dans un nouvel onglet";
	}
	return $atts;
}

/**
 * N'afficher que les sous-menus de la section courante
 */
add_filter( 'wp_nav_menu_objects', 'only_submenu_for_current', 10, 2 );
function only_submenu_for_current( $sorted_menu_items, $args ) {
    $parent_to_leave = array( 0 );
    foreach ( $sorted_menu_items as $item ) {
        if ( 0 != $item->menu_item_parent 
          && ! in_array( $item->menu_item_parent, $parent_to_leave ) 
          && count( array_intersect( $item->classes, array( 
               'current-menu-item',
               'current-menu-parent',
               'current-menu-ancestor' ) ) ) > 0 ) {
            $parent_to_leave[] = $item->menu_item_parent;
        } elseif ( in_array( 'current-menu-item', $item->classes ) ) {
            $parent_to_leave[] = $item->ID;
        }
    }

    // 2nd passe
    foreach ( $sorted_menu_items as $key => $item ) {
        if ( ! in_array( $item->menu_item_parent, $parent_to_leave ) ) {
            unset( $sorted_menu_items[ $key ] );
        } elseif ( 0 == $item->menu_item_parent 
                && ! in_array( $item->ID, $parent_to_leave ) ) {
            if ( $class_key = array_search( 'menu-item-has-children', $sorted_menu_items[ $key ]->classes ) ) {
                unset( $sorted_menu_items[ $key ]->classes[ $class_key ] );
            }
        }
    }
    return $sorted_menu_items;
}

/**
 * Faire une liste dynamique
 */
add_action( 'walker_nav_menu_start_el', 'nav_list_cat_on_page_for_posts', 10, 4 );
function nav_list_cat_on_page_for_posts( $item_output, $item, $depth, $args ) {
	// Si l'ID de la page ciblé par le menu est la home du blog
	// page_for_posts est autoload : pas de requête en plus :-3
	if ( get_option( 'page_for_posts' ) == $item->object_id ) {
		$item_output .= '<ul class="sub-menu">';
		$item_output .= wp_list_categories( array(
							'orderby'          => 'count',
							'order'            => 'DESC',
							'number'           => 5, // juste les 5 plus remplies
							'echo'             => false,
		                   ) );
		$item_output .= '</ul>';
	}
	return $item_output;
}

/**
 * Si je préfère utiliser les walkers, mais que je n'ai pas envie
 * d'éplucher ce thème themeforest à la recherche
 * de tous les wp_nav_menu
 */
add_filter( 'wp_nav_menu_args', 'need_walker_texas_ranger' );
function need_walker_texas_ranger( $args ) {
	$args->walker = new Walker_Texas_Ranger();
	return $args;
}

