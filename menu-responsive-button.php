<?php
/**
 * Ajout d'un élément avant le menu
 */
add_action( 'wp_nav_menu', 'responsive_menu_button', 9, 2 );
function responsive_menu_button( $menu, $args ) {
	// S'il s'agit du menu principal, j'ajoute mon bouton devant
	if ( 'menu_primary' == $args->theme_location ) {
		$menu = '<button class="switch-menu" type="button">Menu</button>' . $menu;
	}
	return $menu;
}