<?php
/**
 * Faire une liste dynamique
 */
add_filter( 'walker_nav_menu_start_el', 'nav_list_cat_on_page_for_posts', 10, 4 );
function nav_list_cat_on_page_for_posts( $item_output, $item, $depth, $args ) {
	// Si l'ID de la page ciblé par le menu est la home du blog
	// page_for_posts est autoload : pas de requête en plus :-3
	if ( get_option( 'page_for_posts' ) == $item->object_id ) {
		$item_output .= '<ul class="sub-menu">';
		$item_output .= wp_list_categories( array(
							'orderby'          => 'count',
							'order'            => 'DESC',
							'number'           => 5, // juste les 5 plus remplies
							'echo'             => false,
		                   ) );
		$item_output .= '</ul>';
	}
	return $item_output;
}