<?php
/**
 * Si je préfère utiliser les walkers, mais que je n'ai pas envie
 * d'éplucher ce thème themeforest à la recherche
 * de tous les wp_nav_menu
 */
add_filter( 'wp_nav_menu_args', 'need_walker_texas_ranger' );
function need_walker_texas_ranger( $args ) {
	$args->walker = new Walker_Texas_Ranger();
	return $args;
}