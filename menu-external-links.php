<?php
/**
 * Ouvrir les liens externes dans une nouvelles fenêtre
 */
add_filter( 'nav_menu_link_attributes', 'open_external_nav_link_new_window' );
function open_external_nav_link_new_window( $atts ) {
	// Si l'URL ne commence pas par mon domaine
	if ( ! preg_match( '/^http:\/\/example\.com/', $atts['href'] ) ) {
		$atts['target'] = '_blank';
		$atts['title']  = "Ce lien s'ouvrira dans un nouvel onglet";
	}
	return $atts;
}