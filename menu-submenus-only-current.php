<?php
/**
 * N'afficher que les sous-menus de la section courante
 */
add_filter( 'wp_nav_menu_objects', 'only_submenu_for_current', 10, 2 );
function only_submenu_for_current( $sorted_menu_items, $args ) {
    // Laisser 0, et ajouter si besoin les menus qui ne doivent pas être concernés par ce filtrage
    $parent_to_leave = array( 0 );
    foreach ( $sorted_menu_items as $item ) {
        $id_el = 0 != $item->menu_item_parent ? $item->menu_item_parent : $item->ID;
        if ( ! in_array( $id_el, $parent_to_leave ) 
          && count( array_intersect( $item->classes, array( 
               'current-menu-item',
               'current-menu-parent',
               'current-menu-ancestor' ) ) ) > 0 ) {
            $parent_to_leave[] = $id_el;
        }
    }

    // 2nd passe
    foreach ( $sorted_menu_items as $key => $item ) {
        if ( ! in_array( $item->menu_item_parent, $parent_to_leave ) ) {
            unset( $sorted_menu_items[ $key ] );
        } elseif ( 0 == $item->menu_item_parent 
                && ! in_array( $item->ID, $parent_to_leave ) ) {
            if ( $class_key = array_search( 'menu-item-has-children', $sorted_menu_items[ $key ]->classes ) ) {
                unset( $sorted_menu_items[ $key ]->classes[ $class_key ] );
            }
        }
    }
    return $sorted_menu_items;
}