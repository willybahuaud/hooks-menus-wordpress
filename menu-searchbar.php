<?php
/**
 * Ajouter la barre de recherche
 */
add_filter( 'wp_nav_menu_items', 'nav_menu_add_search', 10, 2 );
function nav_menu_add_search( $items, $args ) {
	if ( 'primary' == $args->theme_location ) {
		$searchbar = '<li><form action="' . home_url( '/' ) . '">';
		$searchbar .= '<input type="search" name="s">';
		$searchbar .= '<input type="submit" value="go">';
		$searchbar .= '</form><li>';

		$items .= $searchbar;
	}
	return $items;
}