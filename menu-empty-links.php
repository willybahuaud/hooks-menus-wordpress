<?php
/**
 * Changer les liens vides en span
 */
add_action( 'walker_nav_menu_start_el', 'empty_nav_links_to_span', 10, 4 );
function empty_nav_links_to_span( $item_output, $item, $depth, $args ) {
	if ( '#' == $item->url || true == $item->current ) {
		$item_output = preg_replace( '/<a.*?>(.*)<\/a>/', '<span>$1</span>', $item_output );
	}
	return $item_output;
}